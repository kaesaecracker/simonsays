package org.kaesaecracker.games.simonsays;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Simonsays extends JFrame {

	public JPanel drawPanel = new SimonSaysJPanel();

	public enum square {

		RED, GREEN, BLUE, YELLOW
	}

	public void start() {

		this.setTitle("Simon Says");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setResizable(false);

		drawPanel.setPreferredSize(new Dimension(402, 402));

		this.add(drawPanel);

		this.pack();
		this.setVisible(true);

	}

	public class SimonSaysJPanel extends JPanel {

		BufferedImage normalImg = new BufferedImage(402, 402, BufferedImage.BITMASK);
		BufferedImage redImg = new BufferedImage(402, 402, BufferedImage.BITMASK);
		BufferedImage blueImg = new BufferedImage(402, 402, BufferedImage.BITMASK);
		BufferedImage yellowImg = new BufferedImage(402, 402, BufferedImage.BITMASK);
		BufferedImage greenImg = new BufferedImage(402, 402, BufferedImage.BITMASK);
		BufferedImage currentImg = new BufferedImage(402, 402, BufferedImage.BITMASK);

		int rounds = 0;

		public SimonSaysJPanel() {

			Graphics g = normalImg.getGraphics();
			g.setColor(Color.red);
			g.fillRect(0, 0, 200, 200);
			g.setColor(Color.green);
			g.fillRect(0, 200, 200, 200);
			g.setColor(Color.blue);
			g.fillRect(200, 0, 200, 200);
			g.setColor(Color.yellow);
			g.fillRect(200, 200, 200, 200);

			g = redImg.getGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, 200, 200);
			g.setColor(Color.red);
			g.drawRect(0, 0, 199, 199);
			g.setColor(Color.green);
			g.fillRect(0, 200, 200, 200);
			g.setColor(Color.blue);
			g.fillRect(200, 0, 200, 200);
			g.setColor(Color.yellow);
			g.fillRect(200, 200, 200, 200);

			g = blueImg.getGraphics();
			g.setColor(Color.red);
			g.fillRect(0, 0, 200, 200);
			g.setColor(Color.green);
			g.fillRect(0, 200, 200, 200);
			g.setColor(Color.white);
			g.fillRect(200, 0, 200, 200);
			g.setColor(Color.blue);
			g.drawRect(200, 0, 199, 199);
			g.setColor(Color.yellow);
			g.fillRect(200, 200, 200, 200);

			g = greenImg.getGraphics();
			g.setColor(Color.red);
			g.fillRect(0, 0, 200, 200);
			g.setColor(Color.white);
			g.fillRect(0, 200, 200, 200);
			g.setColor(Color.green);
			g.drawRect(0, 200, 199, 199);
			g.setColor(Color.blue);
			g.fillRect(200, 0, 200, 200);
			g.setColor(Color.yellow);
			g.fillRect(200, 200, 200, 200);

			g = yellowImg.getGraphics();
			g.setColor(Color.red);
			g.fillRect(0, 0, 200, 200);
			g.setColor(Color.green);
			g.fillRect(0, 200, 200, 200);
			g.setColor(Color.blue);
			g.fillRect(200, 0, 200, 200);
			g.setColor(Color.white);
			g.fillRect(200, 200, 200, 200);
			g.setColor(Color.yellow);
			g.drawRect(200, 200, 199, 199);

			currentImg = normalImg;

			//<editor-fold defaultstate="collapsed" desc="mouse listener">
			this.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					int x = e.getX();
					int y = e.getY();
					
					if (x < 201) {
						if (y < 201) {
							onRedClick();
							blinkRed();
						} else {
							onGreenClick();
							blinkGreen();
						}
					} else {
						if (y < 201) {
							onBlueClick();
							blinkBlue();
						} else {
							onYellowClick();
							blinkYellow();
						}
					}
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
				}
				
				@Override
				public void mouseReleased(MouseEvent e) {
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
				}
				
			});
			//</editor-fold>
		}

		public void blinkRed() {
			currentImg = redImg;
			repaint();

			Timer timer = new Timer(500, null);
			timer.start();

			timer.addActionListener((ActionEvent e) -> {
				currentImg = normalImg;
				repaint();
				timer.stop();
			});
		}

		public void blinkGreen() {
			currentImg = greenImg;
			repaint();

			Timer timer = new Timer(500, null);
			timer.start();

			timer.addActionListener((ActionEvent e) -> {
				currentImg = normalImg;
				repaint();
				timer.stop();
			});
		}

		public void blinkBlue() {
			currentImg = blueImg;
			repaint();

			Timer timer = new Timer(500, null);
			timer.start();

			timer.addActionListener((ActionEvent e) -> {
				currentImg = normalImg;
				repaint();
				timer.stop();
			});
		}

		public void blinkYellow() {
			currentImg = yellowImg;
			repaint();

			Timer timer = new Timer(500, null);
			timer.start();

			timer.addActionListener((ActionEvent e) -> {
				currentImg = normalImg;
				repaint();
				timer.stop();
			});
		}

		@Override
		public void paintComponent(Graphics g) {
			g.drawImage(currentImg, 0, 0, null);
		}

	}

	public static void main(String[] args) throws InterruptedException {
		Simonsays sisa = new Simonsays();
		sisa.start();
	}

	private void onRedClick() {
		
	}

	private void onGreenClick() {
		
	}

	private void onBlueClick() {
		
	}

	private void onYellowClick() {
		
	}

}
